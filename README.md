# Terraform CodePipeline

no info yet.



### Prerequisites

The list of software needed.

* Git
* Terraform v0.12.12
* AWS Cli
* provider.aws v2.33.0
* provider.template v2.1.2

### Installing

AWS CLI will be needed to be configured on your machine where you are planning to deploy, Terraform uses those AWS crendetials and provision the resources.

If you haven't set up your AWS CLI Credentials on your machine please follow the next official documentation from AWS - https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html, How to set up your crendentials here : https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html.

Clone the repository from bitbucket

```
git clone 
```

Execute initialization command on terraform, `init` command is used to initialize a working directory containing Terraform configuration files. 

```
Terraform init
```


Terraform `plan` to view AWS resources about to be created.

```
Terraform plan
```

If there are no errors you can proceed with the actual resource provitioning

```
Terraform apply
```

Write "yes" to continue.


## Built With

* [Terraform](https://www.terraform.io/) - Infrastructure As A Code to provision and manage any cloud, infrastructure or code
* [Amazon Web Services](https://aws.amazon.com/) - Cloud Provider



## Authors

* **Angel Castro**