resource "aws_codebuild_project" "terraform_build_project" {
  name         = "terraform-build-project"
  description  = "CodeCommit project to execute Terraform commands"
  service_role = "${aws_iam_role.codebuild-role.arn}"

  # Information about the build output artifacts for the build project.
  # https://docs.aws.amazon.com/codebuild/latest/APIReference/API_ProjectArtifacts.html
  artifacts {
    type = "CODEPIPELINE"
  }

  # Information about the build environment of the build project.
  # https://docs.aws.amazon.com/codebuild/latest/APIReference/API_ProjectEnvironment.html
  environment {
    # The type of build environment to use for related builds.
    # Available values are: LINUX_CONTAINER or WINDOWS_CONTAINER.
    type = "LINUX_CONTAINER"

    # Information about the compute resources the build project uses. Available values include:
    #
    # - BUILD_GENERAL1_SMALL: Use up to 3 GB memory and 2 vCPUs for builds.
    # - BUILD_GENERAL1_MEDIUM: Use up to 7 GB memory and 4 vCPUs for builds.
    # - BUILD_GENERAL1_LARGE: Use up to 15 GB memory and 8 vCPUs for builds.
    #
    # BUILD_GENERAL1_SMALL is only valid if type is set to LINUX_CONTAINER
    compute_type = "BUILD_GENERAL1_SMALL"

    # The image identifier of the Docker image to use for this build project.
    # https://docs.aws.amazon.com/codebuild/latest/userguide/build-env-ref-available.html
    image = "aws/codebuild/amazonlinux2-x86_64-standard:1.0"

    # Enables running the Docker daemon inside a Docker container.
    # Set to true only if the build project is be used to build Docker images,
    # and the specified build environment image is not provided by AWS CodeBuild with Docker support.
    # Otherwise, all associated builds that attempt to interact with the Docker daemon fail.
    # privileged_mode = "${var.privileged_mode}"
  }

  # Information about the build input source code for the build project.
  # https://docs.aws.amazon.com/codebuild/latest/APIReference/API_ProjectSource.html
  source {
    type = "CODEPIPELINE"

    # The build spec declaration to use for this build project's related builds.
    # If you include a build spec as part of the source code, by default,
    # the build spec file must be named buildspec.yml and placed in the root of your source directory.
    # https://docs.aws.amazon.com/codebuild/latest/userguide/build-spec-ref.html#build-spec-ref-name-storage
    # buildspec = "${var.buildspec}"
  }

  # Information about the cache for the build project.
  # https://docs.aws.amazon.com/codebuild/latest/APIReference/API_ProjectCache.html

  # The KMS customer master key (CMK) to be used for encrypting the build output artifacts.
  # You can specify either the Amazon Resource Name (ARN) of the CMK or,
  # if available, the CMK's alias (using the format alias/alias-name ).
  #
  # If set empty string, CodeBuild uses the AWS-managed CMK for Amazon S3 in your AWS account.
  # https://docs.aws.amazon.com/codebuild/latest/userguide/setting-up.html#setting-up-kms
  # encryption_key = "${var.encryption_key}"

  # How long in minutes, from 5 to 480 (8 hours), for AWS CodeBuild to wait until timing out
  # any related build that does not get marked as completed.
  # build_timeout = "${var.build_timeout}"

  # A mapping of tags to assign to the resource.
  # tags = "${merge(map("Name", var.name), var.tags)}"
}